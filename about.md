---
title: About
---
<br />

# What is Dynare ?

Dynare is a software platform for handling a wide class of economic models, in particular dynamic stochastic general equilibrium (DSGE) and overlapping generations (OLG) models. The models solved by Dynare include those relying on the rational expectations hypothesis, wherein agents form their expectations about the future in a way consistent with the model. But Dynare is also able to handle models where expectations are formed differently: on one extreme, models where agents perfectly anticipate the future; on the other extreme, models where agents have limited rationality or imperfect knowledge of the state of  the economy and, hence, form their expectations through a learning process. In terms of types of agents, models solved  by Dynare can incorporate consumers, productive firms, governments, monetary authorities,  investors and financial intermediaries. Some degree of heterogeneity can be achieved by including several distinct classes of agents in each of the aforementioned agent categories.

Dynare offers a user-friendly and intuitive way of describing these models. It is able to perform simulations of the model given a calibration of the model parameters and is also able to estimate these parameters given a dataset. In practice, the user will write a text file containing the list of model variables, the dynamic equations linking these variables together,  the computing tasks to be performed and the desired graphical or numerical outputs.

A large panel of applied mathematics and computer science techniques are internally employed by Dynare: multivariate nonlinear solving and optimization, matrix factorizations, local functional approximation, Kalman filters and smoothers, MCMC techniques for Bayesian estimation, graph algorithms, optimal control, etc. References to the literature can be found [here](/bibliography).

Various public bodies (central banks, ministries of economy and finance, international organisations) and some private financial institutions use Dynare for performing policy analysis exercises and as a support tool for forecasting exercises. In the academic world, Dynare is used for research and teaching purposes in postgraduate macroeconomics courses.


# Team

The Dynare project is hosted at <i class="fas fa-external-link-alt"></i>
[Cepremap](http://www.cepremap.org), 48 boulevard Jourdan, 75014 Paris,
France. Development is undertaken by a core team of researchers who devote part
of their time to software development.

<div class="dynare_team_row">
{% for team_member in site.data.dynare_team %}
   {% assign mod = forloop.index0|modulo:5 %}
   {% if mod == 0 or forloop.first %}
   {% endif %}
<div class="dynare_team_column">
{% if team_member.webpage %}<a href="{{ team_member.webpage }}">{% endif %}<img class="avatar" src="{{ team_member.img }}" alt="Photo of {{ team_member.name }}"/>
<p>{{ team_member.name }}
<br>{{ team_member.affiliation }}</p>{% if team_member.webpage %}</a>{% endif %}
</div>
   {% if forloop.last %}
   {% if mod == 0 %}
<div class="dynare_team_column "></div><div class="dynare_team_column "></div><div class="dynare_team_column "></div><div class="dynare_team_column "></div>
   {% elsif mod == 1 %}
<div class="dynare_team_column "></div><div class="dynare_team_column "></div><div class="dynare_team_column "></div>
   {% elsif mod == 2 %}
<div class="dynare_team_column "></div><div class="dynare_team_column "></div>
   {% elsif mod == 3 %}
<div class="dynare_team_column "></div>
   {% endif %}
   {% endif %}
   {% if mod == 4 or forloop.last %}
   {% endif %}
{% endfor %}
   </div>
