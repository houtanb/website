#!/usr/bin/env bash

set -e

cd _includes

for branch in release snapshot
do
    mkdir -p "$branch"
    cd "$branch"

    wget --no-verbose "https://www.dynare.org/$branch/macosx.json"
    wget --no-verbose "https://www.dynare.org/$branch/source.json"
    wget --no-verbose "https://www.dynare.org/$branch/windows.json"
    wget --no-verbose "https://www.dynare.org/$branch/windows-zip.json"

    arr=("macosx" "source" "windows" "windows-zip")

    for dir in "${arr[@]}"
    do
        readarray -t filenames < <(jq .[].filename "$dir.json")
        readarray -t timestamps < <(jq .[].date "$dir.json")
        n=$(jq length "$dir.json")
        includeFilename="download-$branch-$dir.html"
        echo "" > "$includeFilename"
        for ((i = 0; i < n; i++))
        do
            filename="${filenames[i]%\"}"
            filename="${filename#\"}"
            if [ "$branch" = "release" ]; then
                IFS='-' read -ra split <<< "$filename"
                if [ "$dir" = "windows" ] || [ "$dir" = "windows-zip" ]; then
                    version="${split[1]}"
                    ext=${filename##*.}
                elif [ "$dir" = "macosx" ]; then
                    version=${split[1]%.*}
                    ext=${filename##*.}
                else
                    ext="$(echo "${split[1]}" | rev | cut -d. -f2,1 | rev)"
                    version="$(echo "${split[1]}" | cut -d. -f1,2,3)"
                    echo "{% assign dynare_stable_version = \"$version\" %}" > ../dynare_stable_version.md
                fi
                {
                    echo "<a href=\"https://www.dynare.org/$branch/$dir/$filename\">Dynare $version ($ext)</a>"
                    echo "<a href=\"https://www.dynare.org/$branch/$dir/$filename.sig\">[signature]</a>"
                } >> "$includeFilename"
            else
                timestamp="${timestamps[i]%\"}"
                timestamp="${timestamp#\"}"
                if [[ "$OSTYPE" == "darwin"* ]]; then
                    datestr="$(date -r "$timestamp")"
                else
                    datestr="$(date -d @"$timestamp")"
                fi
                {
                    echo "<div class=\"download_row\" onclick=\"document.location = 'https://www.dynare.org/$branch/$dir/$filename'\" onkeypress=\"document.location = 'https://www.dynare.org/$branch/$dir/$filename'\">"
                    echo "  <div class=\"download_cell_left\"><i class=\"fas fa-file-download\"></i>&nbsp;$datestr</div>"
                    echo "  <div class=\"download_cell_right\"><a href=\"#\">$filename</a></div>"
                    echo "</div>"
                } >> "$includeFilename"
            fi
        done
    done
    rm -- *json*
    cd ..
done
