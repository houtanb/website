---
title: Resources
---
<br />

# <i class="fab fa-discourse"></i> Forum

If you run into problems with Dynare that you can't seem to resolve by looking
at the manual, the [Dynare Forum](https://forum.dynare.org/) is a good place to
turn. If you're a beginner, odds are someone else has run into the same problem
as you, so make sure you search the forum for similar questions. Otherwise,
feel free to make an account and ask a question yourself. If you're a more
experienced user, heading to the forum from time to time will allow you to
learn the intricacies of Dynare and you might just be able to help out someone
else out!

# <i class="fas fa-envelope"></i> Mailing List

If you would like to be informed of recent Dynare news by email (new releases,
Summer School, and Conference dates), feel free to [sign up to receive
notifications](https://www.dynare.org/cgi-bin/mailman/listinfo/info). This is a
low traffic list; you'll receive at least 2 and on average 4 emails per year,
depending on how many releases we create.


# <i class="fas fa-graduation-cap"></i> Summer School

We organize a weeklong summer school every year in June where we teach students
how to use Dynare. The fee for students is minimal (equal to expenses on
lunches and one dinner) so as to encourage participation. If this interests
you, please keep a look out for the opening of the application period in April.


# <i class="fas fa-tachometer-alt"></i> Quick Start

For those who want to quickly have something running, the [Quick Start
page](quick_start) explains how to setup Dynare on Windows and to run your
first model file.


# <i class="fas fa-book"></i> Manual

The Dynare Manual is the best place to go to understand the Dynare syntax,
commands, and options. The `.pdf` and `.html` versions of the manual are
distributed with each Dynare release and also available here:

<p class="indent" markdown="1">
<i class="fas fa-file"></i> [HTML Manual](http://www.dynare.org/manual/)
</p>
<p class="indent" markdown="1">
<i class="fas fa-file-download"></i> [PDF Manual](http://www.dynare.org/manual.pdf)
</p>

# <i class="fas fa-book"></i> Dynare Team Presentations

<p class="indent" markdown="1">
<i class="fas fa-file-download"></i> [Sébastien Villemot's Perfect Foresight slides](https://git.dynare.org/sebastien/perfect-foresight-slides/raw/master/deterministic.pdf?inline=false)
</p>

<p class="indent" markdown="1">
<i class="fas fa-file-download"></i> [Sébastien Villemot's Intro to DSGE modeling (in French)](https://git.dynare.org/sebastien/dsge-intro/raw/master/dsge-intro.pdf?inline=false)
</p>


# <i class="fas fa-book"></i> Dynare implementations of published models

<p class="indent">
<i class="fas fa-external-link-alt"></i> <a href="https://sites.google.com/site/ambropo/dynarecodes">Ambrogio Cesa-Bianchi's collection</a>
</p>
<p class="indent">
Replication of
</p>
<ul class="resources">
 <li> Bernanke, Gertler, and Gilchrist, 1999. "The Financial Accelerator in a Quantitative Business Cycle Framework," NBER Working Papers 6455</li>
 <li>Carlstrom and Fuerst, 1997. "Agency Costs, Net Worth, and Business Fluctuations: A Computable General Equilibrium Analysis," American Economic Review,  vol. 87(5), pages 893-910, December</li>
 <li>Schmitt-Grohe, Stephanie & Uribe, Martin, 2003. "Closing small open economy models," Journal of International Economics, vol. 61(1), pages 163-185, October</li>
 </ul>

<p class="indent" markdown="1">
<i class="fas fa-external-link-alt"></i> [Macroeconomic Model Data Base](http://www.macromodelbase.com/)
</p>
<p class="indent" markdown="1">
The database now covers more than 100 models, ranging from small-, medium- and
large-scale DSGE models to earlier-generation New-Keynesian models with
rational expectations and more traditional Keynesian-style models with adaptive
expectations. It includes models of the United States, the Euro Area, Canada,
and several small open emerging economies. Some of the models explicitly
incorporate financial frictions.
</p>

<p class="indent" markdown="1">
<i class="fas fa-external-link-alt"></i> [Johannes Pfeifer's collection of Dynare models](https://github.com/johannespfeifer/dsge_mod)
</p>
<p class="indent" markdown="1">
A collection of Dynare models that demonstrates Dynare best practices,
providing mod files to replicate important models.
</p>


# <i class="fas fa-toolbox"></i> Dynare Toolboxes

<p class="indent" markdown="1">
<i class="fas fa-external-link-alt"></i> [Ambrogio Cesa-Bianchi's toolbox for the analysis of DSGE models estimated with Bayesian techniques](https://sites.google.com/site/ambropo/MatlabCodes)
</p>
<p class="indent" markdown="1">
This toolbox uses the standard output of Dynare to: (i) plot the Markov chain
Monte Carlo (MCMC), (ii) plot the ergodic distribution of the posterior
distribution, (iii) plot the prior versus the posterior distribution, together
with the mode of the posterior, (iv) assess the convergence of the MCMC chain
through CUSUM procedure, and (v) compare the correlation between the estimated
parameters implied by both the Hessian and the chain. The toolbox makes use of
few Matlab routines of Dynare 4.3.
</p>

<p class="indent" markdown="1">
<i class="fas fa-external-link-alt"></i> [Occbin](https://github.com/lucaguerrieri/occbin)
</p>
<p class="indent" markdown="1">
A set of routines that solve models with occasionally binding constraints using Dynare.
</p>

# <i class="fas fa-book"></i> Tutorial
Learn how to perform a stochastic simulation of a small model with Dynare in ten minutes!
<p class="indent" markdown="1">
<i class="fas fa-file-download"></i> [Tutorial](https://www.dynare.org/assets/tutorial/guide.pdf)
</p>
<p class="indent" markdown="1">
<i class="fas fa-file-download"></i> [example1.mod](https://www.dynare.org/assets/tutorial/example1.mod)
</p>
<p class="indent" markdown="1">
<i class="fas fa-file-download"></i> [example2.mod](https://www.dynare.org/assets/tutorial/example2.mod)
</p>


# <i class="fab fa-wikipedia-w"></i> Wiki

Various bits of information such as [new](https://git.dynare.org/Dynare/dynare/wikis/NewFeatures) or undocumented features, [development plans](https://git.dynare.org/Dynare/dynare/wikis/RoadMap), and [known](https://git.dynare.org/Dynare/dynare/wikis/KnownBugs) and [fixed](https://git.dynare.org/Dynare/dynare/wikis/FixedBugs) bugs.
<p class="indent" markdown="1">
<i class="fas fa-file"></i> [New Wiki](https://git.dynare.org/Dynare/dynare/wikis/home)
</p>
<p class="indent" markdown="1">
<i class="fas fa-file"></i> [Old Wiki](http://www.dynare.org/DynareWiki/TableOfContents)
</p>

# <i class="fas fa-book"></i> Dynare++

Originally written by Ondra Kamenik, Dynare++ is a C++ library that computes
higher order approximations of DSGE models via a perturbation method. It is
available as a standalone executable program (shipped with the Dynare
installer) and is also used by Dynare to compute third order approximations.

<p class="indent" markdown="1">
<i class="fas fa-file-download"></i>
[Solving Stochastic Dynamic Equilibrium Models: A K-order Perturbation Approach](https://www.dynare.org/assets/dynare++/k_order.pdf),
a paper by Michel Juillard and Ondra Kamenik that describes the overall algorithm
</p>
<p class="indent" markdown="1">
<i class="fas fa-file-download"></i>
the
[Dynare++ tutorial](https://www.dynare.org/assets/dynare++/dynare++-tutorial.pdf), that
describes the usage of the standalone executable
</p>
<p class="indent" markdown="1">
<i class="fas fa-file-download"></i>
a description of the algorithm used for [Ramsey optimal policy in Dynare++](https://www.dynare.org/assets/dynare++/dynare++-ramsey.pdf)
</p>
<p class="indent" markdown="1">
<i class="fas fa-file-download"></i>
a description of the algorithm used for [solving the specialized Sylvester equation](https://www.dynare.org/assets/dynare++/sylvester.pdf)
</p>
<p class="indent" markdown="1">
<i class="fas fa-file-download"></i>
the internal documentation of the [numerical integration module](https://www.dynare.org/assets/dynare++/integ.pdf)
</p>
<p class="indent" markdown="1">
<i class="fas fa-file-download"></i>
the internal documentation of the [tensor library](https://www.dynare.org/assets/dynare++/tl.pdf)
</p>
<p class="indent" markdown="1">
<i class="fas fa-file-download"></i>
the internal documentation of the [k-order library](https://www.dynare.org/assets/dynare++/kord.pdf)
</p>
