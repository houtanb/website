---
title: Contributing
---
<br />

# How can you contribute to the Dynare Project ?

As a <i class="fas fa-external-link-alt"></i> [free and open-source
software](https://en.wikipedia.org/wiki/Free_and_open-source_software) project,
we think of Dynare as a community. Though primary development is undertaken to
varying degrees by a [core team](/about#team), we encourage others to be
involved in any way that interests them. That could be anything from attending
the [Dynare Conference](#come-to-the-conference), to participating actively in
the [Dynare Forum](#participate-actively-in-the-forum), to spotting and
[reporting bugs](#reporting-bugs), to directly making [changes to the
codebase](#code-development). [Financial
contributions](#financial-contributions) also help ensure the continued
stability of the Dynare project.

## Project Contributions

### Come to the Conference

Every year we organize the Dynare Conference, usually in the
summer. Participation is encouraged even if your paper does not use Dynare. The
application period opens in May and will be announced on this website.

### Participate actively in the forum

The [Dynare Forum](https://forum.dynare.org/) is a great resource to turn to
when you're stuck. Along with the manual, it should be a point of reference
when working with Dynare.

As an advanced user, your contribution will be useful in several ways. First
and foremost, you'll be helping out others who are just beginning on the same
path that you're already on. Second, you may be able to bring a perspective to
the table that's different than that of a beginner and that of someone on the
[core team](/about#team). Finally, your participation will help foster a larger
sense of community and will hopefully allow you gain a deeper understanding of
something you already understand well.


### Reporting Bugs

If you find a bug, we want to know about it! But, first, before you tell us
about it, make sure it's a real bug. Head over to the [Dynare
Forum](https://forum.dynare.org/) and search for the problem you've
encountered; maybe someone else has encountered the same issue and what you
think is a bug is actually intentional behavior. If there's nothing on the
forum and you're not certain it's a bug, please post a question there. We'll
see it and respond accordingly. If, however, you're sure it's a real bug,
please report it on our Gitlab Issue page. But! Before you do, ensure you are
working with the latest version of Dynare.

- If reporting a bug in the ***stable*** version of Dynare:
<p class="indent" markdown="1">Ensure the bug exists in the [latest stable
version of Dynare](/download#stable) telling us exactly how to reproduce the
problem.</p>
- If reporting a bug in the ***unstable*** version of Dynare:
<p class="indent" markdown="1">Ensure the bug exists in the most recent version
of the Dynare source code. You can either test it by installing the latest
[Dynare snapshot](/download#snapshot) or by compliing the source code from the
[Git repository](https://git.dynare.org/Dynare/dynare).</p>

If you have encountered a bug in the stable version of Dynare, there's a chance
we have already fixed it. You can see a list of bugs fixed on the [fixed bugs
Wiki page](https://git.dynare.org/Dynare/dynare/wikis/FixedBugs). There's also
a chance the bug has already been reported. You can see a list of reported bugs
by checking both the [known bugs Wiki
page](https://git.dynare.org/Dynare/dynare/wikis/KnownBugs) and the [Gitlab Issue
page](https://git.dynare.org/Dynare/dynare/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=bug).

Once you have ensured you're working with the correct version of Dynare and
that this bug has not yet been reported, report the bug on our [Gitlab Issue
page](https://git.dynare.org/Dynare/dynare/issues), providing all the code
necessary (`.mod` files, `.m` files, data) to reproduce the bug. If working
with a larger `.mod` file, please pare it down as much as possible. This will
help us find the bug more quickly.


### Fixing Bugs

Even more helpful than finding a bug is to take a crack at fixing it
yourself. First things first though, please [report the bug](#reporting-bugs)!
In your report, if you'd like to fix it as well, please indicate as much. We'll
take a look at the bug and tell you if it's ok to proceed or if we prefer to
fix it ourselves.

### Code development

We track the issues we're working on our [Gitlab Issue
page](https://git.dynare.org/Dynare/dynare/issues). You can organize those
issues by the tags associated with them
(e.g. [bug](https://git.dynare.org/Dynare/dynare/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=bug),
[enhancement](https://git.dynare.org/Dynare/dynare/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=enhancement),
[documentation](https://git.dynare.org/Dynare/dynare/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=documentation),
etc.). An individual issue may have an assignee associated with it. If you find
an issue you'd like to work on that doesn't have an assignee, feel free to
manifest your interest in working on it. Further, if it does have an assignee
but the issue itself is very old, let us know you're interested in working on
it too.

To get started, assuming you have Git installed on your machine you can obtain
the latest development version of the Dynare source code with the following
command:

{% highlight text %}git clone --recurse-submodules https://git.dynare.org/Dynare/dynare.git{% endhighlight %}

As Dynare is <i class="fas fa-external-link-alt"></i> [free
software](https://en.wikipedia.org/wiki/Free_and_open-source_software) licensed
under the <i class="fas fa-external-link-alt"></i> [GNU General Public License
(GPL)](https://www.gnu.org/licenses/gpl-3.0.en.html), any enhancements you make
to the Dynare codebase would need to be under GPLv3+ or some other compatible
license (e.g. public domain) so that it could be merged back into Dynare.

## Financial Contributions

Contrary to proprietary software, as a <i class="fas fa-external-link-alt"></i>
[free and open-source
software](https://en.wikipedia.org/wiki/Free_and_open-source_software)
platform, we do not receive funding by licensing the use of Dynare. That means
that Dynare will remain a public good that will always be free to download,
free to use, and free to modify.

As we don't receive revenue from licensing the use of Dynare (and as we run the
summer school at cost so as not to set up barriers to participation), we depend
on financing from various institutions. That financing is often linked either
to improvements in the Dynare source code itself or the maintenance of Dynare
when used in a production enviornment where responsiveness is of the
essence. If you or your institution would like finance an enhancement to Dynare
or would like to create a maintenance contract, please get in touch.
