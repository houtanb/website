---
layout: posts
title:  "Dynare Summer School 2018"
date:   2018-03-13
categories:
  - events
tags:
  - summer school
---

The Dynare Summer School 2018 will be hosted from June 11 to June 15, 2018 by Banque de France.

## Goals of the Summer School

The school will provide an introduction to Dynare and to Dynamic Stochastic General Equilibrium (DSGE) modeling.

The courses will focus on simulation and estimation of DSGE models with Dynare. The school will also be the occasion to introduce the next official major release of Dynare (4.6).

This Summer school is aimed at beginners as well as at more experienced researchers. PhD students are encouraged to participate. Time will be devoted to the (Dynare related) problems encountered by students in their research.

## Application

Interested people should apply by sending an email to school@dynare.org.

Application should be done no later than April 29th, 2018. You will have to attach a CV and a recent research paper (please only send PDFs).We will confirm acceptance the following week (no latter than May 4th, 2018).

People working in organizations member of DSGE-net (Bank of Finland, Banque de France, Capital Group, European Central Bank, Federal Reserve Bank of Atlanta, Norges Bank, Sverige Riksbank, Swiss National Bank) should register via their network representative.

## Registration Fee

Registration fee (including lunches, coffee breaks and one diner): €200 for academics and €1500 for institutional members.

## Workshop Animators

- Stéphane Adjemian (Université du Maine)
- Thomas Brand (Cepremap)
- Michel Juillard (Banque de France)
- Frédéric Karamé (Université du Maine)
- Marco Ratto (JRC)
- Sébastien Villemot

This workshop is organized with the support of Banque de France, CEPREMAP and DSGE-net.

## Preliminary program

Will be available soon.

## Venue

More information will be available soon.

## Workshop Organization

 This is a laptop only workshop. Each participant is required to come with his/her laptop computer with MATLAB version 7.5 (R2007b) or above installed. We will provide WiFi access, but participants shouldn't rely on it to access a MATLAB license server at their own institution. As an alternative to MATLAB, it is possible to use GNU Octave 4.2 (free software, compatible with MATLAB syntax).

