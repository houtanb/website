---
layout: posts
title:  "Dynare Summer School 2019"
date:   2019-01-24
categories:
  - events
tags:
  - summer school
---

Dynare Summer School 2019 will be hosted from June 3 to June 7, 2019 by École
Normale Supérieure in Paris, France.

## Goals of the Summer School

The school will provide an introduction to Dynare and to Dynamic Stochastic
General Equilibrium (DSGE) modeling.

The courses will focus on the simulation and estimation of DSGE models with
Dynare. The school will also be the occasion to introduce the next official
major release of Dynare (version 4.6).

The Dynare Summer School is aimed at beginners as well as at more experienced
researchers. PhD students are encouraged to participate. Time will be devoted
to the (Dynare related) problems encountered by students in their research.

## Preliminary program

[Preliminary Program (pdf)](/assets/summer-school/2019/preliminary_program.pdf)

## Application

Interested parties can apply by sending a CV and a recent research paper (PDFs
only) to [school@dynare.org](mailto:school@dynare.org).

Applications should be submitted no later than April 7, 2019. We will confirm
acceptance by April 11, 2019.

People working in organizations that are members of DSGE-net (Bank of Finland,
Banque de France, Capital Group, European Central Bank, Federal Reserve Bank of
Atlanta, Norges Bank, Sverige Riksbank, Swiss National Bank) should register
via their network representative.

## Registration Fee

The registration fee (which includes lunches, coffee breaks, and one diner) is
€200 for academics and €1500 for institutional members.

## Invited Speaker

- [Thomas Winberry](http://faculty.chicagobooth.edu/thomas.winberry/)
  (University of Chicago Booth School of Business)

Thomas will present his Dynare toolbox for estimating heterogenous agent models

## Animators

- [Stéphane Adjemian](https://stepan.adjemian.eu/) (Université du Maine)
- [Houtan Bastani](https://www.dynare.org/houtan/) (Cepremap)
- [Michel Juillard](https://ideas.repec.org/e/pju1.html#research) (Banque de France)
- [Frédéric Karamé](http://f.karame.free.fr/index.php) (Université du Maine)
- [Marco Ratto](https://ideas.repec.org/f/pra217.html#research) (European Commission Joint Research Centre)
- [Sébastien Villemot](http://sebastien.villemot.name/) (Cepremap)

This workshop is organized with the support of École Normale Supérieure,
CEPREMAP, Banque de France, and DSGE-net.

## Venue

École Normale Supérieure<br />
48 boulevard Jourdan<br />
75014 Paris<br />
France

## Workshop Organization

This is a laptop only workshop. Each participant is required to come with
his/her laptop with MATLAB R2009b or later installed. We will provide WiFi
access, but participants shouldn't rely on it to access a MATLAB license server
at their own institution. As an alternative to MATLAB, it is possible to use
GNU Octave (free software, compatible with MATLAB syntax).

