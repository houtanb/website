---
layout: posts
title:  "Course: Identification analysis and global sensitivity analysis for Macroeconomic Models."
date:   2019-06-28
categories:
  - events
tags:
  - GSA Workshop
---

Ispra (VA), Italy from Nov. 20 to Nov. 22 2019.

The scope of the course is to give a general introduction to methods of identification and global sensitivity analysis, their DYNARE implementation (identification toolbox and global sensitivity analysis toolbox) and their application to Dynamic Stochastic General Equilibrium (DSGE) macroeconomic models. The course will also give a general introduction of Dynare and on the estimation of DSGE models, also providing an overview of techniques on how to improve the computational efficiency of the estimation with Dynare.  More information and preliminary program available at:

[https://ec.europa.eu/jrc/en/event/workshop/dynare-gsa-macroeconomic-models-2019](https://ec.europa.eu/jrc/en/event/workshop/dynare-gsa-macroeconomic-models-2019)

The course is organised by the Joint Research Centre (JRC) of the European Commission.  Organizers: Marco Ratto, Ludovic Calés, Roberta Cardani, Olga Croitorov, Fabio Di Dio, Lorenzo Frattarolo and Massimo
Giovannini.  External Speakers: Michel Juillard (Banque de France), Stéphane Adjemian (Univ. Maine), Willi Mutschler (Univ. Münster), Johannes Pfeifer (Univ. Köln).  For application, please follow the link:

[https://ec.europa.eu/eusurvey/runner/JRCMACROGSACOURSE2019](https://ec.europa.eu/eusurvey/runner/JRCMACROGSACOURSE2019)
