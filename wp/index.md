---
title: Working Papers
---
<br />

# Latest Dynare Working Papers
{% for wp in site.data.wp %}
{% assign wpn = wp.number %}
{% break %}
{% endfor %}

<ol>
{% for wp in site.data.wp limit:5 %}
<li value = "{{ wpn }}">
  {{ wp.authors }} ({{ wp.year }}), "<a href="{{ wp.url }}">{{ wp.title }}</a>"
  {% if wp.source_url %}(<a href="{{ wp.source_url }}">source</a>){% endif %}
  {% if wp.data_url %}(<a href="{{ wp.data_url }}">data</a>){% endif %}
  {% assign wpn = wpn | minus:1 %}
</li>
{% endfor %}
</ol>
<a href="list">More Dynare Working Papers...</a>

# Purpose and scope of the series

The Dynare Working Papers series is intended for the following kind of contributions:

- methodological papers related to current or forthcoming Dynare functionality;
- papers in quantitative macroeconomics whose results (or portion thereof) have been obtained with the help of Dynare.

Authors are encouraged to submit papers falling into one of the two aforementioned categories, even if they have already been published in another working papers series.

Note that contributions not directly related to Dynare, but still pertaining to the fields of numerical methods or of quantitative macroeconomics, are also welcome.

# Publication and dissemination

The editorial committee of the series is composed of:

- Stéphane Adjemian
- Michel Juillard
- Ferhat Mihoubi
- Sébastien Villemot

Submissions should be sent by e-mail to: [wp@dynare.org](mailto:wp@dynare.org).

**The source code of computer programs used in the paper should also be included in the submission** (this covers Dynare MOD files and/or any other program, whether written in MATLAB, Octave, Fortran, C, C++, etc). If the submission is accepted, the source code will be made publicly available on the present website.

Also, if you use Dynare in your paper, please cite the reference manual as explained on the home page.

We commit ourselves to giving a fast and fair editorial decision, but please forgive us if you do not receive a full-fledged referee report in return.

Papers published in the series are automatically added to the <i class="fas fa-external-link-alt"></i> [RePEc](https://ideas.repec.org/s/cpm/dynare.html) database.

Though this is not a requirement, we encourage our contributors to distribute their work under a free license. For the text of research articles, we recommend using the <i class="fas fa-external-link-alt"></i> [GNU Free Documentation License (FDL)](https://www.gnu.org/licenses/licenses.html#FDL) (for example, this is the licence used for the Dynare documentation and for the material of the Wikipedia encyclopedia). For programs, we recommend using the <i class="fas fa-external-link-alt"></i> [GNU General Public License (GPL)](https://www.gnu.org/licenses/licenses.html#GPL) (for example, this is the license used for Dynare itself and for most parts of the Linux operating system). Note that using these licenses is not equivalent to putting your work in the public domain: you retain the intellectual ownership of your work, and you must be given proper credit if somebody makes use of your work. See the <i class="fas fa-external-link-alt"></i> [GNU website](https://www.gnu.org/licenses/licenses.html) for more details on these licenses and for instructions on how to apply them to your work (you can also look at Dynare source code and at the first Dynare Working Papers for usage examples).
