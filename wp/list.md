---
title: Working Papers
---
<br />

# Dynare Working Papers
{% for wp in site.data.wp %}
{% assign wpn = wp.number %}
{% break %}
{% endfor %}

<ol>
{% for wp in site.data.wp %}
<li value = "{{ wpn }}">
  {{ wp.authors }} ({{ wp.year }}), "<a href="{{ wp.url }}">{{ wp.title }}</a>"
  {% if wp.source_url %}(<a href="{{ wp.source_url }}">source</a>){% endif %}
  {% if wp.data_url %}(<a href="{{ wp.data_url }}">data</a>){% endif %}
  {% assign wpn = wpn | minus:1 %}
</li>
{% endfor %}
</ol>
