# The Dynare Website

## Pre-requisites

- [Ruby](https://www.ruby-lang.org/en/)
- [Jekyll](https://jekyllrb.com/)
- Latex distribution (for compiling logo image with tikz) and pdf2svg

## Setup Instructions

### Jekyll and necessary Gems

- clone this repository: `git clone git@git.dynare.org:Dynare/website.git`
- `cd website`
- `bundle install`

### Create logo images

- `cd assets/images/logo; ./makelogo.sh; cd -`

### Mount website

- `bundle exec jekyll serve`

### Setup the files needed for the Download page

- Fill the `assets/snapshot` folder with the nightly snapshots and checksums
- When done, you will have four subfolders:
  - `assets/snapshot/macosx`
  - `assets/snapshot/source`
  - `assets/snapshot/windows`
  - `assets/snapshot/windows-zip`

### Setup the files needed for the Working Papers page

- `cd assets/RePEc`
- Fill the directory with the `.rdf` files that RePEc needsd
- Run `python rdf2yml.py`

## To push version to server

- `JEKYLL_ENV=production bundle exec jekyll serve`
- push the contents of `_site` folder to server
